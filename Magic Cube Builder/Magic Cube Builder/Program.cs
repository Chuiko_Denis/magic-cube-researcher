﻿using System;
using System.Diagnostics;
using MagicCubeBuilder.MagicCubeBuilding;
using MagicCubeBuilder.MagicCubePrinting;
using MagicCubeBuilder.MagicCubeValidation;

namespace MagicCubeBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Provide cube size (as a fast example 3)");

            var cubeSizeTextRepresentation = Console.ReadKey().KeyChar;
            Console.WriteLine();
            var cubeSize = int.Parse(cubeSizeTextRepresentation.ToString());

            var magicCubesSearcher = new OptimizedMagicCubesSearcher(1, cubeSize, new StrictCubeValidator());
            var magicCubePrinter = new CubesSearchResultToConsolePrinter(magicCubesSearcher);

            var cubesSearchStatusReporter = new CubesSearchStatusReporter(magicCubesSearcher);

            cubesSearchStatusReporter.StartMonitoring();
            var stopWatch = Stopwatch.StartNew();
            var result = magicCubesSearcher.Search();
            stopWatch.Stop();
            cubesSearchStatusReporter.StopMonitoring();

            magicCubePrinter.AddSearchSummary(stopWatch.Elapsed);

            Console.WriteLine($"Search has been complete. Time spent for search: {stopWatch.Elapsed.TotalSeconds:F2}");
            Console.ReadKey();
        }
    }
}
