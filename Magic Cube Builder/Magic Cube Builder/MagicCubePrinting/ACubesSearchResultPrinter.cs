﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MagicCubeBuilder.MagicCubeBuilding;

namespace MagicCubeBuilder.MagicCubePrinting
{
    abstract class ACubesSearchResultPrinter : ICubesSearchResultPrinter
    {
        readonly MagicCubesSearcher _magicCubesSearcher;

        public ACubesSearchResultPrinter(MagicCubesSearcher magicCubesSearcher)
        {
            _magicCubesSearcher = magicCubesSearcher;
            _magicCubesSearcher.MagicCubeWasFoundEvent += Print;
        }

        public abstract void Print(IEnumerable<Cube> cubes);

        public abstract void Print(Cube theCube);

        public abstract void AddSearchSummary(TimeSpan timeSpent);

        protected string GetSearchSummary(TimeSpan timeSpent)
        {
            var result = new StringBuilder();

            result.AppendLine($"Combinations registered: {_magicCubesSearcher.TotalCombinationsPassed}");
            result.AppendLine($"Time spent: {timeSpent.TotalSeconds} seconds");

            return result.ToString();
        }
    }
}
