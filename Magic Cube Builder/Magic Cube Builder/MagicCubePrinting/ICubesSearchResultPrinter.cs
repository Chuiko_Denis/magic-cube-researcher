﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MagicCubeBuilder.MagicCubeBuilding;

namespace MagicCubeBuilder.MagicCubePrinting
{
    interface ICubesSearchResultPrinter
    {
        void Print(IEnumerable<Cube> cubes);
        void Print(Cube theCube);
        void AddSearchSummary(TimeSpan timeSpent);
    }
}
