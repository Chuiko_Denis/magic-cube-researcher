﻿using MagicCubeBuilder.MagicCubeBuilding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicCubeBuilder.MagicCubePrinting
{
    class CubesSearchResultToFilePrinter : ACubesSearchResultPrinter
    {
        readonly string _filePath;

        public CubesSearchResultToFilePrinter(MagicCubesSearcher magicCubesSearcher, string fileDestinationPath)
            : base(magicCubesSearcher)
        {
            _filePath = $"{fileDestinationPath}\\SearchResult_{DateTime.Now.ToString("yy-MM-dd_hhmmss")}.txt";
        }

        public override void Print(IEnumerable<Cube> theCube)
        {
            var cubesAsString = new StringBuilder();

            foreach (var cube in theCube)
                cubesAsString.AppendLine(cube.ToString());

            WriteContentToFile(cubesAsString.ToString());
        }

        public override void Print(Cube theCube)
        {
            WriteContentToFile(theCube.ToString());
        }

        public override void AddSearchSummary(TimeSpan timeSpent)
        {
            WriteContentToFile(base.GetSearchSummary(timeSpent));
        }

        void WriteContentToFile(string contentAsString)
        {
            using (var fileStream = new FileStream(_filePath, FileMode.Append, FileAccess.Write))
                using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
                {
                    streamWriter.WriteLine(contentAsString);
                    streamWriter.Flush();
                }
        }
    }
}
