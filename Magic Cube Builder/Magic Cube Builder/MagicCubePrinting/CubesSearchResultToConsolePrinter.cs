﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MagicCubeBuilder.MagicCubeBuilding;

namespace MagicCubeBuilder.MagicCubePrinting
{
    internal class CubesSearchResultToConsolePrinter : ACubesSearchResultPrinter
    {
        public CubesSearchResultToConsolePrinter(MagicCubesSearcher magicCubesSearcher)
            : base(magicCubesSearcher)
        {   
        }

        public override void Print(IEnumerable<Cube> cubes)
        {
            foreach (var cube in cubes)
                Console.WriteLine(cube.ToString());
        }

        public override void Print(Cube theCube)
        {
            Console.WriteLine(theCube.ToString());
        }

        public override void AddSearchSummary(TimeSpan timeSpent)
        {
            Console.WriteLine(base.GetSearchSummary(timeSpent));
        }
    }
}
