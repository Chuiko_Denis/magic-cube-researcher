﻿namespace MagicCubeBuilder.MagicCubeBuilding
{
    class LockableValue
    {
        public LockableValue(int value)
        {
            Value = value;
        }

        public int Value { get; }

        public bool IsLocked { get; set; }

        public override string ToString()
        {
            return $"{Value} Locked = {IsLocked}";
        }
    }
}
