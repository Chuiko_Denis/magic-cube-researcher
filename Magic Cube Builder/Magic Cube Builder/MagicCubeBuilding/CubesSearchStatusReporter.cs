﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MagicCubeBuilder.MagicCubeBuilding
{
    class CubesSearchStatusReporter
    {
        readonly MagicCubeBuilder.MagicCubeBuilding.MagicCubesSearcher _magicCubeBuilder;
        CancellationTokenSource _cancellationTokenSource;
        CancellationToken _monitorCancellationToken;

        public CubesSearchStatusReporter(MagicCubesSearcher magicCubeBuilder)
        {
            _magicCubeBuilder = magicCubeBuilder;
        }

        public void StartMonitoring()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _monitorCancellationToken = _cancellationTokenSource.Token;
            var task = Task.Factory.StartNew(MonitorBuilderStatus, _monitorCancellationToken);
        }

        public void StopMonitoring()
        {
            _cancellationTokenSource.Cancel();
        }

        void MonitorBuilderStatus()
        {
            while (true)
            {
                Thread.Sleep(5000);
                if (_monitorCancellationToken.IsCancellationRequested)
                    return;
                var progress = 100 * Math.Round((double)_magicCubeBuilder.TotalCombinationsPassed / _magicCubeBuilder.PossibleCombinationsAmount, 6);
                Console.WriteLine($"Processed {_magicCubeBuilder.TotalCombinationsPassed}. Progress {progress:P} Found {_magicCubeBuilder.TargetsFound}. {DateTime.Now.ToString("HH:mm:ss,ssss")}.");
            }
        }
    }
}
