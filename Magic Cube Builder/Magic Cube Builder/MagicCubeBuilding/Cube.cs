﻿using System.Text;

namespace MagicCubeBuilder.MagicCubeBuilding
{
    public class Cube
    {
        readonly int _size;
        readonly int[,] _cubeMatrix;

        public Cube(int size)
        {
            _size = size;
            _cubeMatrix = new int[_size, _size];
        }

        public int this[int index]
        {
            get => _cubeMatrix[index / _size, index % _size];
            set => _cubeMatrix[index / _size, index % _size] = value;
        }

        public int this[int x, int y]
        {
            get => _cubeMatrix[x, y];
            set => _cubeMatrix[x, y] = value;
        }

        public int Size => _size;

        public static Cube Clone(Cube prototype)
        {
            var result = new Cube(prototype._size);

            for(int x = 0; x < prototype._size; x++)
                for (int y = 0; y < prototype._size; y++)
                    result[x, y] = prototype[x, y];

            return result;
        }

        public int MagicCubeFactor
        {
            get
            {
                var result = 0;

                var midleLine = _size % 2;
                for (int x = 0; x < _size; x++)
                    result += this[x, midleLine];

                return result;
            }
        }

        public override string ToString()
        {
            var reportBuilder = new StringBuilder();
            var cubeSize = this.Size;

            reportBuilder.AppendLine($"Magic cube with size {cubeSize} and \"Magic factor\"{this.MagicCubeFactor}");
            for (int i = 0; i < cubeSize; i++)
            {
                var cubeRowRepresentation = new StringBuilder();
                for (int j = 0; j < cubeSize; j++)
                    cubeRowRepresentation.AppendFormat("{0000}", this[i, j]);

                reportBuilder.AppendLine(cubeRowRepresentation.ToString());
            }

            reportBuilder.AppendLine();

            return reportBuilder.ToString();
        }
    }
}
