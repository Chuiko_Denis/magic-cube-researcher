﻿using System;
using System.Collections.Generic;
using System.Linq;
using MagicCubeBuilder.Extensions;
using MagicCubeBuilder.MagicCubeValidation;

namespace MagicCubeBuilder.MagicCubeBuilding
{
    public delegate void MagicCubeWasFoundDelegate(Cube cube);

    class MagicCubesSearcher
    {
        protected readonly int _lowerValue;
        protected readonly int _cubeSize;
        protected readonly int _lockableValuesArraySize;
        protected readonly ICubeValidator _cubeValidator;
        protected readonly List<Cube> _cubes = new List<Cube>();

        public event MagicCubeWasFoundDelegate MagicCubeWasFoundEvent;

        public MagicCubesSearcher(int lowerValue, int cubeSize, ICubeValidator cubeValidator)
        {
            _lowerValue = lowerValue;
            _cubeSize = cubeSize;
            _lockableValuesArraySize = _cubeSize * _cubeSize;
            _cubeValidator = cubeValidator;

            PossibleCombinationsAmount = (cubeSize * cubeSize).Factorial();
        }

        public long PossibleCombinationsAmount { get; }

        public int TargetsFound => _cubes.Count;

        public long TotalCombinationsPassed { get; protected set; }

        public List<Cube> Search()
        {
            _cubes.Clear();
            TotalCombinationsPassed = 0;

            var valuesArray = BuildSimplyIncreasingValuesCollection(_lowerValue, _lockableValuesArraySize);

            FulfillCube(valuesArray);

            return _cubes;
        }

        IEnumerable<LockableValue> BuildSimplyIncreasingValuesCollection(int lowerValue, int collectionSize)
        {
            var result = new LockableValue[collectionSize];
            
            for (int i = 0; i < collectionSize; i++)
            {
                result[i] = new LockableValue(i + lowerValue);
            }

            return result;
        }

        protected virtual void FulfillCube(IEnumerable<LockableValue> valuesCollection, Cube cube = null, int currentBase = 0)
        {
            cube = cube ?? new Cube(_cubeSize);

            var currentCollectionOfAwailableValues = valuesCollection.Where(i => !i.IsLocked).ToList();
            foreach (var targetValue in currentCollectionOfAwailableValues)
            {
                targetValue.IsLocked = true;
                cube[currentBase] = targetValue.Value;

                if (currentCollectionOfAwailableValues.Count > 1)
                    FulfillCube(valuesCollection, cube, currentBase + 1);
                else
                    AddCubeToCollectionIfValid(cube);

                targetValue.IsLocked = false;
            }
        }

        void AddCubeToCollectionIfValid(Cube cube)
        {
            if (_cubeValidator.IsValid(cube))
            {
                _cubes.Add(Cube.Clone(cube));
                OnCubeFound(cube);
            }

            TotalCombinationsPassed++;
        }

        void OnCubeFound(Cube cube)
        {
            MagicCubeWasFoundEvent?.Invoke(cube);
        }
    }
}
