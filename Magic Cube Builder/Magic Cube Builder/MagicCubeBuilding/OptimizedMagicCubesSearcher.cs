﻿using System;
using System.Collections.Generic;
using System.Linq;
using MagicCubeBuilder.Extensions;
using MagicCubeBuilder.MagicCubeValidation;

namespace MagicCubeBuilder.MagicCubeBuilding
{
    class OptimizedMagicCubesSearcher : MagicCubesSearcher
    {
        public OptimizedMagicCubesSearcher(int lowerValue, int cubeSize, ICubeValidator cubeValidator)
            : base(lowerValue, cubeSize, cubeValidator)
        {
        }

        protected override void FulfillCube(IEnumerable<LockableValue> valuesCollection, Cube cube = null, int currentBase = 0)
        {
            if (currentBase > _cubeSize * 2 &&
                currentBase % _cubeSize == 0
                && !_cubeValidator.IsPriliminaryValid(cube, currentBase / _cubeSize))
            {
                RegisterLeftVariantsAsProcessed(valuesCollection.Count(i => !i.IsLocked));
                return;
            }

            base.FulfillCube(valuesCollection, cube, currentBase);
        }

        void RegisterLeftVariantsAsProcessed(int leftUnlockedItems)
        {
            this.TotalCombinationsPassed += leftUnlockedItems.Factorial();
        }
    }
}
