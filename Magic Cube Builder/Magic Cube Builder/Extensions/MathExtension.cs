﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicCubeBuilder.Extensions
{
    static class MathExtension
    {
        public static long Factorial(this int baseNumber)
        {
            long result = 1;

            for (int i = 1; i <= baseNumber; i++)
                result *= i;

            return result;
        }
    }
}
