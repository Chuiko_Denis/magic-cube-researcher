﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MagicCubeBuilder.MagicCubeBuilding;

namespace MagicCubeBuilder.MagicCubeValidation
{
    class StrictCubeValidator : StandardCubeValdator
    {
        public override bool IsValid(Cube cube)
        {
            if (!base.IsValid(cube)) 
                return false;

            if (!IsCorrectForRows(cube))
                return false;

            if (!IsCorrectForColumns(cube))
                return false;

            return true;
        }

        bool IsCorrectForColumns(Cube cube)
        {
            int? verificationResult = null;

            for (int x = 0; x < cube.Size; x++)
            {
                var result = 0;
                for (int y = 0; y < cube.Size; y++)
                    result += cube[x, y];

                if (!verificationResult.HasValue)
                    verificationResult = result;

                if (result != verificationResult)
                    return false;
            }

            return true;
        }

        bool IsCorrectForRows(Cube cube)
        {
            int? verificationResult = null;

            for (int y = 0; y < cube.Size; y++)
            {
                var result = 0;
                for (int x = 0; x < cube.Size; x++)
                    result += cube[x, y];

                if (!verificationResult.HasValue)
                    verificationResult = result;

                if (result != verificationResult)
                    return false;
            }

            return true;
        }
    }
}
