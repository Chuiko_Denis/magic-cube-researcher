﻿using MagicCubeBuilder.MagicCubeBuilding;

namespace MagicCubeBuilder.MagicCubeValidation
{
    class MiddleValueDefinedStrictCubeValidator : StrictCubeValidator
    {
        readonly int _targetMiddleCellValue;

        public MiddleValueDefinedStrictCubeValidator(int targetMiddleCellValue)
        {
            _targetMiddleCellValue = targetMiddleCellValue;
        }

        public override bool IsValid(Cube cube)
        {
            if (!HasCorrectShape(cube)) return false;

            int middleLine = cube.Size / 2;

            var result = cube[middleLine, middleLine] == _targetMiddleCellValue;

            return result && base.IsValid(cube);
        }
    }
}
