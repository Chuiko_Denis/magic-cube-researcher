﻿using MagicCubeBuilder.MagicCubeBuilding;

namespace MagicCubeBuilder.MagicCubeValidation
{
    class MiddleValueDefinedCubeValidator : StandardCubeValdator
    {
        readonly int _targetMiddleCellValue;

        public MiddleValueDefinedCubeValidator(int targetMiddleCellValue)
        {
            _targetMiddleCellValue = targetMiddleCellValue;
        }

        public override bool IsValid(Cube cube)
        {
            if (!HasCorrectShape(cube)) return false;

            int middleLine = cube.Size / 2;

            var result = cube[middleLine, middleLine] == _targetMiddleCellValue;

            return result && base.IsValid(cube);
        }
    }
}
