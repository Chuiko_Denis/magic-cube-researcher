﻿using System.Collections.Generic;
using MagicCubeBuilder.MagicCubeBuilding;

namespace MagicCubeBuilder.MagicCubeValidation
{

    class StandardCubeValdator : ICubeValidator
    {
        public virtual bool IsValid(Cube cube)
        {
            var result = true;

            if (!HasCorrectShape(cube)) return false;

            var sumForMiddleRow = SumForMiddleRow(cube);
            var sumForMiddleColumn = SumForMiddleColumn(cube);
            var sumForUpperLeftToBottomRightDiagonal = SumForUpperLeftToBottomRightDiagonal(cube);
            var sumForBottomLeftToUpperRightDiagonal = SumForBottomLeftToUpperRightDiagonal(cube);

            if (sumForMiddleRow != sumForMiddleColumn
                || sumForMiddleColumn != sumForUpperLeftToBottomRightDiagonal
                || sumForUpperLeftToBottomRightDiagonal != sumForBottomLeftToUpperRightDiagonal)
                return false;

            return result;
        }

        public bool HasCorrectShape(Cube cube) => cube.Size % 2 == 1;

        int SumForMiddleRow(Cube cube)
        {
            var result = 0;
            var midianIndex = cube.Size / 2;

            for (int x = 0; x < cube.Size; x++)
                result += cube[x, midianIndex];

            return result;
        }

        int SumForMiddleColumn(Cube cube)
        {
            var result = 0;
            var midianIndex = cube.Size / 2;

            for (int y = 0; y < cube.Size; y++)
                result += cube[midianIndex, y];

            return result;
        }

        int SumForUpperLeftToBottomRightDiagonal(Cube cube)
        {
            var result = 0;

            for (int z = 0; z < cube.Size; z++)
                result += cube[z, z];

            return result;
        }

        int SumForBottomLeftToUpperRightDiagonal(Cube cube)
        {
            var result = 0;

            for (int z = 0; z < cube.Size; z++)
                result += cube[cube.Size - 1 - z, z];

            return result;
        }

        public bool IsPriliminaryValid(Cube cube, int latestRowToCheck)
        {
            var lastRegisteredSum = 0;

            for (int y = 0; y < latestRowToCheck; y++)
            {
                var newRowSum = 0;
                for (int x = 0; x < cube.Size; x++) 
                    newRowSum += cube[x, y];

                if (lastRegisteredSum == 0)
                    lastRegisteredSum = newRowSum;
                else if (newRowSum != lastRegisteredSum)
                    return false;
            }

            return true;
        }
    }
}
