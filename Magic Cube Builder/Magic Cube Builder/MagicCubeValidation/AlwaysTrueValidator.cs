﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MagicCubeBuilder.MagicCubeBuilding;

namespace MagicCubeBuilder.MagicCubeValidation
{
    class AlwaysTrueValidator : ICubeValidator
    {
        public bool IsValid(Cube cube) => true;
        public bool IsPriliminaryValid(Cube cube, int latestRowToCheck) => true;
    }
}
