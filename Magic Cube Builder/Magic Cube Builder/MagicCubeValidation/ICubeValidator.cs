﻿using MagicCubeBuilder.MagicCubeBuilding;

namespace MagicCubeBuilder.MagicCubeValidation
{
    interface ICubeValidator
    {
        bool IsValid(Cube cube);
        bool IsPriliminaryValid(Cube cube, int latestRowToCheck);
    }
}
